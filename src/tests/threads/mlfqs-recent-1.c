/* Verifies that lowering a thread's priority so that it is no
 *   longer the highest-priority thread in the system causes it to
 *   yield immediately. */
#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/init.h"
#include "threads/synch.h"
#include "threads/thread.h"
static thread_func changing_thread;
static thread_func changing_thread2;

void
test_mlfqs_recent_1 (void)
{
    /* This test does not work with the MLFQS. */
    ASSERT (selected_scheduler != SCH_MLFQS);
    
    msg ("Creando varios threads.");
    thread_create ("thread 1", PRI_DEFAULT, changing_thread, NULL);
    msg("creado thread 1");
    thread_create("thread 2",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 2");
    thread_create("thread 3",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 3");
    thread_create("thread 4",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 4");
    thread_create("thread 5",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 5");
    thread_create("thread 6",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 6");
    thread_create("thread 7",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 7");
    thread_create("thread 8",PRI_DEFAULT,changing_thread,NULL);
    msg("creado thread 8");
    
    
    msg("ahora esta el thread %s",thread_name());
    
}
void changing_thread (void *aux UNUSED)
{
    int i=0;
    int a=0;
    int c=0;
    for(i=0;i<32000;i++)
    {
        
        if(a<31999){
            a=a+1;
        }
        
        else
        {
            msg("%s termino el ciclo",thread_name());
                  
        }
                
    }
      
        msg ("Thread %s exiting.",thread_name());
}